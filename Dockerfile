FROM ruby:2.7.1
RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
RUN mkdir /rails_auth
WORKDIR /rails_auth
COPY Gemfile /rails_auth/Gemfile
COPY Gemfile.lock /rails_auth/Gemfile.lock
RUN bundle install
COPY . /rails_auth

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
